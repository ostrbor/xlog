package xlog

import (
	"bytes"
	"errors"
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func BenchmarkMsgWrite(b *testing.B) {
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			_ = Msg("no context").Write("")
		}
	})
}

func TestFailed(t *testing.T) {
	HOSTNAME = "hostname"
	ENVIRONMENT = "production"
	body := bytes.NewBufferString(`{"body_field":"body_value"}`)
	req, err := http.NewRequest(http.MethodPost, "http://localhost:8080/?query_name=query_value", body)
	check(err)
	reqBody, err := io.ReadAll(req.Body)
	check(err)
	req.Header.Set("Content-Type", "application/json")

	e := Failed("test", errors.New("new error")).
		RefID("123").
		User("user_name").
		Ctx(C{"data": "context_data"}).
		Req(req.Method, req.URL.String(), req.Header, reqBody).
		Resp(200, req.URL.String(), nil, nil)
	l := e.String()
	if !strings.Contains(l, "failed test") {
		t.Error("message format")
	}
	if !strings.Contains(l, "123") {
		t.Error("no reference")
	}
	if !strings.Contains(l, "user_name") {
		t.Error("no user name")
	}
	if !strings.Contains(l, "new error") {
		t.Error("no err")
	}
	if !strings.Contains(l, "context_data") {
		t.Error("no context")
	}
	if !strings.Contains(l, "hostname") {
		t.Error("no hostname")
	}
	if !strings.Contains(l, "body_field") {
		t.Error("no request body field")
	}
	if !strings.Contains(l, "body_value") {
		t.Error("no request body field value")
	}
	if !strings.Contains(l, "POST") {
		t.Error("no request method")
	}
	if !strings.Contains(l, "localhost:8080") {
		t.Error("no request host")
	}
	if !strings.Contains(l, "query_name") {
		t.Error("no request query name")
	}
	if !strings.Contains(l, "query_value") {
		t.Error("no request query value")
	}
	if !strings.Contains(l, "Content-Type") {
		t.Error("no request header name")
	}
	if !strings.Contains(l, "application/json") {
		t.Error("no request header value")
	}
	if !strings.Contains(l, "200") {
		t.Error("no response code")
	}
	if !strings.Contains(l, "production") {
		t.Error("no environment")
	}
}

type panicWriter struct{}

func (w *panicWriter) Write([]byte) (int, error) {
	panic("panicWriter")
}

func TestWriteRecover(t *testing.T) {
	defer func() {
		if r := recover(); r != nil {
			t.Error("panic in Write must not be propagated")
		}
		Writer = stdout{}
	}()
	Writer = &panicWriter{}
	check(Msg("force writer to panic").Write(""))
}

func TestCallerSkip(t *testing.T) {
	event := Failed("", nil)
	if !strings.HasSuffix(event.Caller.File, "log_test.go") {
		t.Error("wrong caller file: fix skip value in setCaller")
	}
}

func TestHttpWriter_Write(t *testing.T) {
	defer func() {
		Writer = stdout{}
	}()
	logSent := false
	headersSet := false
	handler := func(w http.ResponseWriter, r *http.Request) {
		logSent = true
		ct := r.Header.Get("Content-Type")
		auth := r.Header.Get("Authorization")
		if ct == "application/json" && auth == "Bearer token" {
			headersSet = true
		}
		w.WriteHeader(200)
	}
	ts := httptest.NewServer(http.HandlerFunc(handler))
	defer ts.Close()
	Writer = HttpWriter{URL: ts.URL, Headers: map[string]string{"Authorization": "Bearer token"}}
	err := Msg("send msg").Write("")
	if err != nil {
		t.Error("failed: ", err)
	}
	if !logSent {
		t.Error("log must be sent")
	}
	if !headersSet {
		t.Error("headers must be set")
	}
}

func TestHttpWriter_tries(t *testing.T) {
	defer func() {
		Writer = stdout{}
	}()
	tries := 0
	handler := func(w http.ResponseWriter, r *http.Request) {
		switch tries {
		case 0:
			w.WriteHeader(400)
			w.Write([]byte(`{"error":"failed"}`))
		case 1:
			w.WriteHeader(500)
		default:
			w.WriteHeader(200)
		}
		tries++
		return
	}
	ts := httptest.NewServer(http.HandlerFunc(handler))
	defer ts.Close()
	Writer = HttpWriter{URL: ts.URL}
	_, err := Writer.Write([]byte(`{"message":"test"}`))
	if err != nil {
		t.Error("failed: ", err)
	}
	if tries != 3 {
		t.Error("want tries=3; got ", tries)
	}
}
