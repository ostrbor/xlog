package xlog

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"runtime"
	"strings"
	"text/tabwriter"
	"time"
)

var (
	BodyMaxBytes           = 3 * 1 << 10
	Writer       io.Writer = stdout{}
	HOSTNAME, _            = os.Hostname()
	ENVIRONMENT            = ""
)

func Init(environment string, writer io.Writer, bodyMaxBytes int) {
	ENVIRONMENT = environment
	if writer != nil {
		Writer = writer
	}
	if bodyMaxBytes != 0 {
		BodyMaxBytes = bodyMaxBytes
	}
}

type Event struct {
	Message string `json:"message"`

	// Should be set by log sender. Format: RFC3339, UTC timezone.
	LogTimestamp string `json:"log_timestamp"`

	// Use cases:
	//  - to track connected chain of events,
	//  - user can reference specific request, as reference id is supposed to be sent to user in case of errors.
	ReferenceID string `json:"reference_id,omitempty"`

	// Most of the requests are not anonymous.
	User_ string `json:"user,omitempty"`

	// Short name of the error, without details.
	// Used by notifiers: if error field is set then notifier will send notification.
	// Type is string, because structure errorString is not serializable (there are no public fields).
	Error string `json:"error,omitempty"`

	// String representation of function arguments, constants...
	// No need to save numbers as integers:
	//   - log database is unlikely to be used to calculate values.
	// No need to serialize structures as JSON:
	//   - many structures are not intended to be represented as JSON,
	//   - it's unlikely to query log database by structure field.
	// For structure serializing prefer "%#v". NOTE: values of reference type are not readable!
	Context map[string]string `json:"context,omitempty"`

	Request  string `json:"request,omitempty"`
	Response string `json:"response,omitempty"`

	// In kubernetes HOSTNAME is equal to pod's name.
	// In docker-compose use 'hostname' param to set HOSTNAME  inside container.
	Hostname string `json:"hostname,omitempty"`

	// production or staging
	Environment string `json:"environment,omitempty"`

	// There are many functions with the same name.
	// Message with a name of just a function is not enough to find a place of error in code.
	Caller *caller `json:"caller,omitempty"`
}

// shortcut for Context type
type C map[string]string

func newEvent(m string) *Event {
	return &Event{
		Message:      m,
		LogTimestamp: time.Now().UTC().Format(time.RFC3339Nano),
		Hostname:     HOSTNAME,
		Environment:  ENVIRONMENT,
	}
}

func Failed(fn string, err error) *Event {
	e := newEvent("failed " + fn)
	setCaller(e, 2)
	if err != nil {
		e.Error = err.Error()
	}
	return e
}

func Msg(m string) *Event {
	return newEvent(m)
}

func Msgf(format string, a ...interface{}) *Event {
	m := fmt.Sprintf(format, a...)
	return newEvent(m)
}

func Panicked(fn string) *Event {
	e := newEvent("panicked " + fn)
	setCaller(e, 2)
	return e
}

func (e *Event) String() string {
	return string(marshal(e))
}

func (e *Event) Bytes() []byte {
	return marshal(e)
}

func (e *Event) Print(refID string) {
	e.ReferenceID = refID
	fmt.Println(e)
}

// Clients are not expected to handle logging errors. All Write errors will be written to stdout.
func (e *Event) Write(refID string) error {
	defer func() {
		if r := recover(); r != nil {
			Panicked("Writer.Write").Err(fmt.Errorf("%v", r)).Print(refID)
		}
	}()
	e.ReferenceID = refID
	b := e.Bytes()
	if b != nil {
		if _, err := Writer.Write(b); err != nil {
			Failed("Writer.Write", err).
				Ctx(C{"data": fmt.Sprintf("%q", b)}).
				Print(refID)
			return err
		}
	}
	return nil
}

func (e *Event) RefID(r string) *Event {
	e.ReferenceID = r
	return e
}

func (e *Event) User(u string) *Event {
	e.User_ = u
	return e
}

func (e *Event) Err(err error) *Event {
	if err != nil {
		e.Error = err.Error()
	} else {
		e.Error = "nil"
	}
	if e.Caller == nil {
		setCaller(e, 2)
	}
	return e
}

func (e *Event) Ctx(c C) *Event {
	e.Context = c
	return e
}

// http.Request is not required as argument, because it's client responsibility to read request body and clone body for further use.
// http.Request is complex structure with many fields, only part of which are used in logging.
func (e *Event) Req(method, url string, headers http.Header, body []byte) *Event {
	if method == "" && url == "" && len(headers) == 0 && len(body) == 0 {
		return e
	}
	req := fmt.Sprintf("\n%s %s", method, url)
	if len(headers) > 0 {
		req += "\n"
		req += formatHeaders(headers)
	}
	if len(body) > 0 {
		req += "\n"
		req += formatBody(body)
	}
	e.Request = req
	return e
}

func (e *Event) Resp(statusCode int, url string, headers http.Header, body []byte) *Event {
	if statusCode == 0 && url == "" && len(headers) == 0 && len(body) == 0 {
		return e
	}
	resp := fmt.Sprintf("\n%d %s", statusCode, url)
	if len(headers) > 0 {
		resp += "\n"
		resp += formatHeaders(headers)
	}
	if len(body) > 0 {
		resp += "\n"
		resp += formatBody(body)
	}
	e.Response = resp
	return e
}

type caller struct {
	File string `json:"file,omitempty"`
	Line int    `json:"line,omitempty"`
}

func setCaller(e *Event, skip int) {
	if e.Caller != nil {
		return
	}
	_, file, line, _ := runtime.Caller(skip)
	e.Caller = &caller{
		File: file,
		Line: line,
	}
}

// Marshal errors are not returned, because any error is unlikely.
// There are two possible errors:
//   1 Type error - impossible here, because we do not pass chan or function to Event constructors or field setters.
//   2 Value error - impossible here, because we always pass to Marshal only structure.
// But nevertheless we handle error returned from Marshal by printing it to stdout.
func marshal(e *Event) []byte {
	log, err := json.Marshal(&e)
	if err != nil {
		Failed("json.Marshal", err).
			Ctx(C{"event": fmt.Sprintf("%#v", e)}).
			Print(e.ReferenceID)
		return nil
	}
	return log
}

func formatHeaders(headers http.Header) string {
	buf := new(bytes.Buffer)
	w := tabwriter.NewWriter(buf, 1, 1, 5, ' ', 0)
	for header, values := range headers {
		v := strings.Join(values, ", ")
		fmt.Fprintln(w, fmt.Sprintf("%s:\t%s", header, v))
	}
	w.Flush()
	return buf.String()
}

func formatBody(body []byte) string {
	if len(body) > BodyMaxBytes {
		return string(body[:BodyMaxBytes]) + " ...removed..."
	}
	b := make(map[string]interface{})
	err := json.Unmarshal(body, &b)
	if err != nil {
		return string(body)
	}
	result, err := json.MarshalIndent(b, "", "\t")
	if err != nil {
		return string(body)
	}
	return string(result)
}

type stdout struct{}

// Newline is appended, otherwise all logs will be written as one line.
func (w stdout) Write(p []byte) (n int, err error) {
	n, err = os.Stdout.Write(p)
	if err != nil {
		return
	}
	_, _ = os.Stdout.Write([]byte("\n"))
	return
}

type HttpWriter struct {
	URL     string
	Headers map[string]string
}

func (w HttpWriter) Write(p []byte) (n int, err error) {
	for tries := 0; tries < 3; tries++ {
		err = send(w.URL, p, w.Headers)
		if err == nil {
			return len(p), nil
		}
		time.Sleep(150 * time.Millisecond)
	}
	return 0, err
}

func send(url string, body []byte, headers map[string]string) (err error) {
	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(body))
	if err != nil {
		return
	}
	req.Header.Set("Content-Type", "application/json")
	for k, v := range headers {
		req.Header.Set(k, v)
	}
	c := http.Client{Timeout: 5 * time.Second}
	resp, err := c.Do(req)
	if err != nil {
		return
	}
	if resp.StatusCode < 200 || resp.StatusCode >= 300 {
		return fmt.Errorf("want StatusCode=2**; got %d", resp.StatusCode)
	}
	return nil
}
