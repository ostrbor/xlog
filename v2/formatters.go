package xlog

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"
	"text/tabwriter"
)

const bodyMaxBytes = 10 * 1 << 10

func FormatHeaders(headers http.Header) string {
	if len(headers) == 0 {
		return ""
	}
	buf := new(bytes.Buffer)
	w := tabwriter.NewWriter(buf, 1, 1, 5, ' ', 0)
	if _, err := fmt.Fprintln(w); err != nil {
		log.Println("failed Fprintln: ", err)
		return ""
	}
	for header, values := range headers {
		var value string
		s := strings.ToLower(header)
		switch {
		case s == "authorization" || s == "password" || s == "bearer" || strings.Contains(s, "secret"):
			value = "...removed..."
		default:
			value = strings.Join(values, ", ")
		}
		_, err := fmt.Fprintf(w, "%s:\t%s\n", header, value)
		if err != nil {
			log.Println("failed Fprintln: ", err, "\n", header, value)
		}
	}
	err := w.Flush()
	if err != nil {
		log.Println("failed Flush: ", err, "\n", headers)
	}
	return buf.String()
}

func FormatBody(body []byte) string {
	if len(body) == 0 {
		return ""
	}
	if len(body) > bodyMaxBytes {
		return string(body[:bodyMaxBytes]) + " ...removed..."
	}
	b := make(map[string]any)
	err := json.Unmarshal(body, &b)
	if err != nil {
		return string(body)
	}
	result, err := json.MarshalIndent(b, "", "\t")
	if err != nil {
		log.Println("failed MarshalIndent: ", err, "\n", b)
		return string(body)
	}
	return "\n" + string(result)
}
