package xlog

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestLog_Ctx(t *testing.T) {
	l := Msg("").Ctx("msg", "\n")
	msg := l.Context["msg"]
	if msg == `"\n"` {
		t.Error("must be newline, not how newline is represented in source code")
	}
}

func TestLog_Request(t *testing.T) {
	req := httptest.NewRequest(http.MethodPost, "http://example.com", nil)
	req.Body = nil // caller might log cloned request, clone function might set nil to body
	defer func() {
		if r := recover(); r != nil {
			t.Error("must not panic with nil body")
		}
	}()
	Msg("test").Request(req)
}
