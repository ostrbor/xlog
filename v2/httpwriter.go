package xlog

import (
	"bytes"
	"fmt"
	"log"
	"net/http"
	"time"
)

const (
	timeoutSec    = 5
	maxLoggingSec = 30
)

type HttpWriter struct {
	URL     string
	Headers map[string]string
	Async   bool
}

func (w HttpWriter) Write(p []byte) (n int, err error) {
	if !w.Async {
		return write(w.URL, p, w.Headers)
	}
	go func() {
		defer func() {
			if r := recover(); r != nil {
				log.Printf("panicked: %v\n\t%s\n", r, p)
			}
		}()
		write(w.URL, p, w.Headers)
	}()
	return 0, nil
}

func write(url string, p []byte, headers map[string]string) (n int, err error) {
	deadline := time.Now().Add(maxLoggingSec * time.Second)
	for tries := 0; time.Now().Before(deadline); tries++ {
		err = send(url, p, headers)
		if err == nil {
			return len(p), nil
		}
		time.Sleep(1 << uint(tries) * time.Second)
	}
	fmt.Println("failed http Write: ", err, "\n", string(p))
	return 0, err
}

func send(url string, body []byte, headers map[string]string) (err error) {
	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(body))
	if err != nil {
		return
	}
	req.Header.Set("Content-Type", "application/json")
	for k, v := range headers {
		req.Header.Set(k, v)
	}
	c := http.Client{Timeout: timeoutSec * time.Second}
	resp, err := c.Do(req)
	if err != nil {
		return
	}
	if resp.StatusCode < 200 || resp.StatusCode >= 300 {
		return fmt.Errorf("want StatusCode=2**; got %d", resp.StatusCode)
	}
	return nil
}
