package xlog

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"os"
	"runtime"
	"time"
)

var (
	HOSTNAME, _ = os.Hostname()
	ENVIRONMENT = ""

	// nil cancels logging, use os.Stdout to print logs
	Writer io.Writer = nil
)

func Init(environment string, writer io.Writer) {
	if writer == nil {
		panic("nil writer")
	}
	ENVIRONMENT = environment
	Writer = writer
}

type Log struct {
	Msg_ string `json:"message"`
	Usr_ string `json:"user,omitempty"`

	// To track connected chain of events, to reference request.
	Ref_ string `json:"reference_id,omitempty"`

	// Short name of the error, without details.
	// Used by notifiers: if error field is set then notifier will send notification.
	// Type is string, because structure errorString is not serializable (there are no public fields).
	Error string `json:"error,omitempty"`

	Context map[string]string `json:"context,omitempty"`
	Req_    string            `json:"request,omitempty"`
	Resp_   string            `json:"response,omitempty"`

	// In kubernetes HOSTNAME is equal to pod's name.
	// In docker-compose use 'hostname' param to set HOSTNAME  inside container.
	Hostname string `json:"hostname,omitempty"`

	// production or staging
	Environment string `json:"environment,omitempty"`

	Caller *caller `json:"caller,omitempty"`

	// RFC3339, UTC
	LogTimestamp string `json:"log_timestamp"`
}

type caller struct {
	File string `json:"file,omitempty"`
	Line int    `json:"line,omitempty"`
}

func newLog() Log {
	return Log{
		Hostname:    HOSTNAME,
		Environment: ENVIRONMENT,
		Context:     make(map[string]string),
	}
}

func setCaller(l Log, skip int) Log {
	if l.Caller != nil {
		return l
	}
	_, file, line, _ := runtime.Caller(skip)
	l.Caller = &caller{
		File: file,
		Line: line,
	}
	return l
}

func marshal(l Log) []byte {
	b, err := json.Marshal(&l)
	if err != nil {
		log.Println("failed marshal: ", err, "\n", l)
	}
	return b
}

func (l Log) String() string {
	return string(marshal(l))
}

func (l Log) Bytes() []byte {
	return marshal(l)
}

func (l Log) Write() {
	l.LogTimestamp = time.Now().UTC().Format(time.RFC3339Nano)
	b := l.Bytes()
	if Writer == nil {
		return
	}
	if Writer == os.Stdout {
		fmt.Println(string(b))
		return
	}
	if b != nil {
		if _, err := Writer.Write(b); err != nil {
			log.Println("failed Writer.Write: ", err, "\n", l)
			return
		}
	}
}
