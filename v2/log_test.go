package xlog

import (
	"errors"
	"fmt"
	"testing"
)

func Test(t *testing.T) {
	l := Failed(fmt.Println, errors.New("testerr"))
	if l.Msg_ != "failed fmt.Println" {
		t.Error("invalid msg")
	}
	if l.Error != "testerr" {
		t.Error("invalid err")
	}
}
