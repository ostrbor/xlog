package xlog

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"reflect"
	"runtime"
	"runtime/debug"
	"strings"
)

func Msg(m string) Log {
	l := newLog()
	l.Msg_ = m
	return l
}

func User(u string) Log {
	l := newLog()
	l.Usr_ = u
	return l
}

func Ref(r string) Log {
	l := newLog()
	l.Ref_ = r
	return l
}

func Failed(fn any, err error) Log {
	return failed(newLog(), fn, err)
}

func Panicked(fn any, r any) Log {
	return panicked(newLog(), fn, r)
}

func (l Log) Msg(m string) Log {
	l.Msg_ = m
	return l
}

func (l Log) User(u string) Log {
	l.Usr_ = u
	return l
}

func (l Log) Ref(r string) Log {
	l.Ref_ = r
	return l
}

func (l Log) Failed(fn any, err error) Log {
	return failed(l, fn, err)
}

func (l Log) Panicked(fn any, r any) Log {
	return panicked(l, fn, r)
}

func (l Log) Err(err error) Log {
	if err != nil {
		l.Error = err.Error()
	} else {
		l.Error = "nil"
	}
	if l.Caller == nil {
		l = setCaller(l, 2)
	}
	return l
}

func (l Log) Ctx(kv ...any) Log {
	ctx := make(map[string]string)
	var key string
	for i, x := range kv {
		switch (i + 1) % 2 {
		case 1:
			key = fmt.Sprintf("%v", x)
		case 0:
			switch value := x.(type) {
			case string:
				ctx[key] = value
			default:
				ctx[key] = fmt.Sprintf("%#v", value)
			}
		}
	}
	l.Context = ctx
	return l
}

func (l Log) Req(method, url string, headers http.Header, body []byte) Log {
	if method == "" && url == "" && len(headers) == 0 && len(body) == 0 {
		return l
	}
	req := fmt.Sprintf("\n%s %s", method, url)
	req += FormatHeaders(headers)
	req += FormatBody(body)
	l.Req_ = req
	return l
}

// clone request before calling this method, as it reads body
func (l Log) Request(clone *http.Request) Log {
	if clone == nil {
		return l
	}
	var (
		body []byte
		err  error
	)
	if clone.Body != nil {
		body, err = io.ReadAll(clone.Body)
		if err != nil {
			log.Println("failed ReadAll: ", err, "\n", l)
		}
	}
	return l.Req(clone.Method, clone.URL.String(), clone.Header, body)
}

func (l Log) Resp(statusCode int, headers http.Header, body []byte) Log {
	if statusCode == 0 && len(headers) == 0 && len(body) == 0 {
		return l
	}
	resp := fmt.Sprintf("\n%d", statusCode)
	resp += FormatHeaders(headers)
	resp += FormatBody(body)
	l.Resp_ = resp
	return l
}

func (l Log) Response(clone *http.Response) Log {
	if clone == nil {
		return l
	}
	var (
		body []byte
		err  error
	)
	if clone.Body != nil {
		body, err = io.ReadAll(clone.Body)
		if err != nil {
			log.Println("failed ReadAll: ", err, "\n", l)
		}
	}
	return l.Resp(clone.StatusCode, clone.Header, body)
}

// accepts function type or string(function name)
func fnName(fn any) string {
	if fn != nil {
		kind := reflect.TypeOf(fn).Kind()
		switch kind {
		case reflect.String:
			return fn.(string)
		case reflect.Func:
			return runtime.FuncForPC(reflect.ValueOf(fn).Pointer()).Name()
		default:
		}
	}
	return "<unknown>"
}

func failed(l Log, fn any, err error) Log {
	l.Msg_ = strings.TrimSpace("failed " + fnName(fn))
	l = setCaller(l, 3)
	if err != nil {
		l.Error = err.Error()
	}
	return l
}

func panicked(l Log, fn, r any) Log {
	l.Msg_ = strings.TrimSpace("panicked " + fnName(fn))
	err := fmt.Errorf("%v", r)
	l.Error = err.Error()
	return l.Ctx("stack", string(debug.Stack()))
}
