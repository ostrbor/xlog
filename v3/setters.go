package xlog

import (
	"fmt"
	"reflect"
	"runtime"
)

func New() Log {
	return newLog()
}

func Msg(m string) Log {
	return newLog().Msg(m)
}

func (l Log) Msg(m string) Log {
	l.Msg_ = m
	return l
}

func Trace(t string) Log {
	return newLog().Trace(t)
}

func (l Log) Trace(t string) Log {
	l.TraceID = t
	return l
}

func (l Log) TraceFrom(lg Log) Log {
	l.TraceID = lg.TraceID
	return l
}

func User(u string) Log {
	return newLog().User(u)
}

func (l Log) User(u string) Log {
	l.Usr_ = u
	return l
}

func (l Log) Err(err error) Log {
	if err != nil {
		l.Err_ = err.Error()
	} else {
		l.Err_ = "nil"
	}
	return l
}

func (l Log) Ctx(kv ...any) Log {
	l.Ctx_ = make(map[string]string)
	return l.AddCtx(kv...)
}

func (l Log) AddCtx(kv ...any) Log {
	var key string
	for i, x := range kv {
		switch (i + 1) % 2 {
		case 1:
			key = fmt.Sprintf("%v", x)
		case 0:
			switch val := x.(type) {
			case string:
				l.Ctx_[key] = val
			default:
				l.Ctx_[key] = fmt.Sprintf("%#v", val)
			}
		}
	}
	return l
}

func (l Log) Fn(fn any) Log {
	l.Func = fnName(fn)
	return l
}

func fnName(fn any) string {
	if fn != nil {
		k := reflect.TypeOf(fn).Kind()
		switch k {
		case reflect.String:
			return fn.(string)
		case reflect.Func:
			return runtime.FuncForPC(reflect.ValueOf(fn).Pointer()).Name()
		}
	}
	return "<unknown>"
}
