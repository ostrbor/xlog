package xlog

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/url"
	"os"
	"runtime"
	"time"
)

var (
	HOSTNAME, _           = os.Hostname() // 'hostname' in docker-compose
	ENVIRONMENT           = ""            // 'prod', 'sandbox'
	Writer      io.Writer = nil           // os.Stdout, HttpWriter
	stderr                = log.New(os.Stderr, "xlog: ", log.Flags())
)

func Init(env string, w io.Writer) {
	if w == nil {
		stderr.Panicln("nil writer")
	}
	ENVIRONMENT = env
	Writer = w
}

// todo change json names:
// Msg_: msg, TraceID: trace, Err_: err
type Log struct {
	Msg_    string `json:"message"`
	TraceID string `json:"reference_id,omitempty"`
	Usr_    string `json:"user,omitempty"`
	Pub_    bool   `json:"pub"` // can log be accessed publicly

	Err_ string            `json:"error,omitempty"`
	Func string            `json:"func,omitempty"`
	Ctx_ map[string]string `json:"ctx,omitempty"`

	ReqMethod string `json:"req_method,omitempty"`
	ReqURL    string `json:"req_url,omitempty"`
	ReqHeader string `json:"req_header,omitempty"`
	ReqBody   string `json:"req_body,omitempty"`

	RespStatus int    `json:"resp_status,omitempty"`
	RespHeader string `json:"resp_header,omitempty"`
	RespBody   string `json:"resp_body,omitempty"`

	Host    string `json:"host,omitempty"`
	Env     string `json:"env,omitempty"`
	LogFile string `json:"log_file,omitempty"` // file where log was called
	LogLine int    `json:"log_line,omitempty"` // line where log was called
	LogErr  string `json:"log_err,omitempty"`  // logging client errors
	LogTime string `json:"log_time"`
}

func newLog() Log {
	return Log{
		Host:    HOSTNAME,
		Env:     ENVIRONMENT,
		Ctx_:    make(map[string]string),
		TraceID: UUID(),
	}
}

func setCaller(l *Log, skip int) {
	_, file, line, ok := runtime.Caller(skip)
	if !ok {
		e := "setCaller: not ok"
		addLogErr(l, e)
		stderr.Println(e)
		return
	}
	l.LogFile = file
	l.LogLine = line
}

func marshal(l Log) []byte {
	b, err := json.Marshal(&l)
	if err != nil {
		e := fmt.Sprintf("marshal: %v", err)
		addLogErr(&l, e)
		stderr.Println(e)
	}
	return b
}

func addLogErr(l *Log, e string) {
	if l.LogErr != "" {
		l.LogErr = l.LogErr + ", " + e
	} else {
		l.LogErr = e
	}
}

func (l Log) String() string {
	return string(marshal(l))
}

func (l Log) Bytes() []byte {
	return marshal(l)
}

func (l Log) WritePub() {
	l.Pub_ = true
	l.Write()
}

func (l Log) Write() {
	if l.Pub_ {
		setCaller(&l, 3)
	} else {
		setCaller(&l, 2)
	}
	if l.Msg_ == "" {
		if l.ReqMethod != "" && l.ReqURL != "" {
			var path string
			if p, err := url.Parse(l.ReqURL); err != nil {
				e := fmt.Sprintf("url.Parse: %v", err)
				addLogErr(&l, e)
				path = "/"
			} else {
				path = p.Path
			}
			l.Msg_ = HTTP(l.ReqMethod, path, l.RespStatus)
		}
	}

	l.LogTime = time.Now().UTC().Format(time.RFC3339Nano)
	b := l.Bytes()
	if Writer == nil {
		return
	}
	if Writer == os.Stdout {
		fmt.Println(string(b))
		return
	}
	if b != nil {
		_, _ = Writer.Write(b)
	}
}
