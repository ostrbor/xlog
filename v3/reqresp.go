package xlog

import (
	"bytes"
	"fmt"
	"io"
	"net/http"
	"strings"
)

const bodyMaxBytes = 5 * 1 << 10

func Req(method, url string, header http.Header, body []byte) Log {
	return newLog().Req(method, url, header, body)
}

func (l Log) Req(method, url string, header http.Header, body []byte) Log {
	l.ReqMethod = method
	l.ReqURL = url
	l.ReqHeader = head2str(redact(header))
	if len(body) > bodyMaxBytes {
		e := fmt.Sprintf("req_body: length exceeds %d bytes", bodyMaxBytes)
		addLogErr(&l, e)
		b := body[:bodyMaxBytes]
		l.ReqBody = string(b) + fmt.Sprintf("<cut %d bytes>", len(body)-len(b))
	} else {
		l.ReqBody = string(body)
	}
	return l
}

func Request(r *http.Request) Log {
	return newLog().Request(r)
}

func (l Log) Request(r *http.Request) Log {
	if r == nil {
		return l
	}
	var (
		body []byte
		err  error
	)
	if r.Body != nil {
		body, err = io.ReadAll(r.Body)
		if err != nil {
			e := fmt.Sprintf("Request ReadAll: %v", err)
			addLogErr(&l, e)
			stderr.Println(e)
		}
		r.Body = io.NopCloser(bytes.NewBuffer(body))
	}
	return l.Req(r.Method, r.URL.String(), r.Header, body)
}

func (l Log) Resp(status int, header http.Header, body []byte) Log {
	l.RespStatus = status
	l.RespHeader = head2str(redact(header))
	if len(body) > bodyMaxBytes {
		e := fmt.Sprintf("resp_body: length exceeds %d bytes", bodyMaxBytes)
		addLogErr(&l, e)
		b := body[:bodyMaxBytes]
		l.RespBody = string(b) + fmt.Sprintf("<cut %d bytes>", len(body)-len(b))
	} else {
		l.RespBody = string(body)
	}
	return l
}

func (l Log) Response(resp *http.Response) Log {
	if resp == nil {
		return l
	}
	var (
		body []byte
		err  error
	)
	if resp.Body != nil {
		body, err = io.ReadAll(resp.Body)
		if err != nil {
			e := fmt.Sprintf("Response ReadAll: %v", err)
			addLogErr(&l, e)
			stderr.Println(e)
		}
		resp.Body = io.NopCloser(bytes.NewBuffer(body))
	}
	return l.Resp(resp.StatusCode, resp.Header, body)
}

func head2str(h http.Header) string {
	b := bytes.Buffer{}
	h.Write(&b)
	return b.String()
}

func redact(h http.Header) (c http.Header) {
	c = h.Clone()
	for k := range c {
		edit := true

		switch kl := strings.ToLower(k); {
		case kl == "authorization":
		case strings.Contains(kl, "password"):
		case strings.Contains(kl, "secret"):
		default:
			edit = false
		}

		if edit {
			c[k] = []string{"...redacted..."}
		}
	}
	return c
}
