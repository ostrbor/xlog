package xlog

import (
	"fmt"
	"math/rand"
	"time"
)

const letters = "abcdef0123456789"

func UUID() string {
	r := string(rnd(31))
	return fmt.Sprintf("%s-%s-4%s-%s-%s", r[:8], r[8:12], r[12:15], r[15:19], r[19:])
}

func rnd(s int) []byte {
	b := make([]byte, s)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return b
}

func init() {
	rand.Seed(time.Now().UnixNano())
}
