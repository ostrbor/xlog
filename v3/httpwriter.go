package xlog

import (
	"bytes"
	"fmt"
	"net/http"
	"time"
)

const (
	timeoutSec = 10
	maxLogSec  = 300
)

type HttpWriter struct {
	URL     string
	Headers map[string]string
	Async   bool
}

func (w HttpWriter) Write(p []byte) (n int, err error) {
	if !w.Async {
		return write(w.URL, p, w.Headers)
	}
	go func() {
		defer func() {
			if r := recover(); r != nil {
				stderr.Printf("http Write panic: %v\n", r)
			}
		}()
		_, _ = write(w.URL, p, w.Headers)
	}()
	return 0, nil
}

func write(url string, p []byte, headers map[string]string) (n int, err error) {
	deadline := time.Now().Add(maxLogSec * time.Second)
	for tries := 0; time.Now().Before(deadline); tries++ {
		err = send(url, p, headers)
		if err == nil {
			return len(p), nil
		}
		time.Sleep(10 * time.Second)
	}
	return 0, err
}

func send(url string, body []byte, headers map[string]string) (err error) {
	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(body))
	if err != nil {
		stderr.Printf("send NewRequest: %v\n", err)
		return
	}
	req.Header.Set("Content-Type", "application/json")
	for k, v := range headers {
		req.Header.Set(k, v)
	}
	c := http.Client{Timeout: timeoutSec * time.Second}
	resp, err := c.Do(req)
	if err != nil {
		stderr.Printf("send Do: %v\n; url: %q, headers: %v", err, url, headers)
		return
	}
	if resp.StatusCode < 200 || resp.StatusCode >= 300 {
		err := fmt.Errorf("expected 2xx, got %d", resp.StatusCode)
		stderr.Printf("send status check: %v\n", err)
		return err
	}
	return nil
}
