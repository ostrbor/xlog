package xlog

import (
	"fmt"
	"runtime/debug"
	"strings"
)

func HTTP(method, path string, status int) string {
	msg := fmt.Sprintf("%s %s", method, path)
	if status != 0 {
		msg += fmt.Sprintf(" [%d]", status)
	}
	return msg
}

func Failed(fn any, err error) Log {
	return newLog().Failed(fn, err)
}

func (l Log) Failed(fn any, err error) Log {
	m := strings.TrimSpace("failed " + fnName(fn))
	return l.Msg(m).Err(err)
}

func Panicked(fn any, r any) Log {
	return newLog().Panicked(fn, r)
}

func (l Log) Panicked(fn any, r any) Log {
	m := strings.TrimSpace("panicked " + fnName(fn))
	err := fmt.Errorf("%v", r)
	return l.Msg(m).Err(err).Ctx("stack", string(debug.Stack()))
}
