package xlog

import (
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestLog_Ctx(t *testing.T) {
	l := Msg("").Ctx("a", "\n")
	msg := l.Ctx_["a"]
	if msg == `"\n"` {
		t.Error("must be newline symbol")
	}

	l2 := l.Ctx("b", "new")
	if l2.Ctx_["a"] != "" {
		t.Error("ctx must be reset")
	}
}

func TestLog_AddCtx(t *testing.T) {
	l := Msg("").Ctx("a", "one")

	l2 := l.AddCtx("b", "two")
	if l2.Ctx_["a"] != "one" && l2.Ctx_["b"] != "two" {
		t.Error("ctx must be merged")
	}

	l3 := l.AddCtx("a", "three")
	if l3.Ctx_["a"] != "three" {
		t.Error("old ctx must be replaced with new")
	}
}

func TestLog_Request(t *testing.T) {
	clone := httptest.NewRequest(http.MethodGet, "http://example.com", nil)
	clone.Body = nil // might be nil after cloning
	defer func() {
		if r := recover(); r != nil {
			t.Error("must not panic with nil body")
		}
	}()
	Msg("").Request(clone)
}

func TestLog_Req(t *testing.T) {
	New().Req("", "", nil, nil)
	defer func() {
		if r := recover(); r != nil {
			t.Error("must not panic with zero values")
		}
	}()
}

func TestLog_Response(t *testing.T) {
	clone := &http.Response{Body: nil}
	defer func() {
		if r := recover(); r != nil {
			t.Error("must not panic with nil body")
		}
	}()
	Msg("").Response(clone)
}

func TestLog_Resp(t *testing.T) {
	New().Resp(0, nil, nil)
	defer func() {
		if r := recover(); r != nil {
			t.Error("must not panic with zero values")
		}
	}()
}

func Test_fnName(t *testing.T) {
	fn := fnName(Test_fnName)
	if !strings.HasSuffix(fn, "Test_fnName") {
		t.Error("invalid fnName: ", fn)
	}
}

func TestLog_Err(t *testing.T) {
	l := Msg("").Err(nil)
	if l.Err_ != "nil" {
		t.Error("invalid Err_: ", l.Err_)
	}
}

func Test_redact(t *testing.T) {
	h := make(http.Header)
	h.Set("Authorization", "token")
	h.Set("Password", "passwd")
	h.Set("Client-Secret", "secret")

	h2 := redact(h)
	redacted := "<redacted>"
	if h2.Get("Authorization") != redacted {
		t.Error("authorization must be redacted")
	}
	if h2.Get("Password") != redacted {
		t.Error("password must be redacted")
	}
	if h2.Get("Client-Secret") != redacted {
		t.Error("secret must be redacted")
	}
}

func Test_addLogErr(t *testing.T) {
	l := newLog()
	addLogErr(&l, "a")
	addLogErr(&l, "b")
	if l.LogErr != "a, b" {
		t.Error("invalid log err: ", l.LogErr)
	}
}
