## Installation

`go get -u gitlab.com/ostrbor/xlog`

## Getting Started

```go
package main

import (
    "gitlab.com/ostrbor/xlog"
)

func main() {
    xlog.Msg("hello world").Print("")
}

func init() {
	xlog.Init("production", xlog.HttpWriter{URL:"http://localhost:8080/"}, 0)
}

// Output: {"message":"hello world","timestamp":"2021-01-31T20:38:54.733874815Z","environment":"production"}
```

## ABSTRACT

For the majority of small and medium projects logging of events in JSON format is very simple.
Libraries that provide this functionality are too complicated for such an easy task.

## Advantages

 - **simplicity**
   
'Zap' has ~18000 lines of code, 'zerolog' ~12000. This library is much smaller.
 

Instead of implementing custom encoder to improve performance, here is used standard json encoder.


Instead of implementing sampling, which is reasonable for projects with more than 100 messages per second, this library 
aims at projects with fewer logs per second rate.
   

 - **brevity**
     
```go
package main

import (
	"gitlab.com/ostrbor/xlog"
	"go.uber.org/zap"
)

func main() {
	logger, _ := zap.NewProduction()
	defer logger.Sync()
	logger.Info("failed to fetch URL",
		zap.String("url", url),
		zap.Int("attempt", 3),
		zap.Duration("backoff", time.Second),
	)

	xlog.Failed("fetchURL", err).
		User(user).
		Write("reference_id")
}
```

 - **uniform log structure**

Predefined set of log fields is complete and covers all needs for small and medium projects. It's easy to add a new field using Context.

Uniform logs improves readability.

As fields are predefined, there is no need to spend time on naming each field in a new project.
    

## Disadvantages

 - **performance**
   

## Performance

Result of benchmark test for logging to stdout without context:

```shell BenchmarkLog-8   	    71268	        16751 ns/op```

Logging in zap takes 900 ns/op (20 times faster). 

16900 - 900 = 15 000 ns = 0.02 of millisecond

Each log in a handler will increase latency by 0.02 millisecond.

As we can see performance is not a big deal for most projects.

Also, when error happened there is usually no need to hurry to respond.


## Sampling

Sampling is used to decrease load on CPU and I/O.

In case of 1000 logs per second net throughput will be ~1Mb/sec (1 Kb of log message).

Mongodb can handle more than 10 000 inserts per second.

Therefore, high load of logging is not a problem for most projects with RPS lower than 10 000.

Notification spam can be solved by using cache of messages and errors in notifiers.


## Conclusion

If RPS of project is less than 10 000, there is no need to import complex
logging libraries with advanced functionality. If you do, you won't use half of what 
they offer.
